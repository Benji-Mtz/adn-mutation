import numpy as np
from helpers import detection_mutation, get_diagonal_vectors_adn_mutation, get_row_vectors_adn_mutation, get_col_vectors_adn_mutation

def hasMutation(array):
   
    # set_base_adn_incorrecto = {'C', 'G', 'A', 'T', 'Letra_extra'}
    
    set_base_adn = {'C', 'G', 'A', 'T'}
    
    array_total_string = "".join(array)
    set_based_on_array = set(array_total_string)

    rows = len(array)
    # columns = len(array[0]) 
    # print(rows, columns)

    for val in array:
        if len(val) == rows:
            columns = rows
        else:
            columns = len(val)
            break
        
    
    if (set_base_adn == set_based_on_array):
        if rows == columns:    
            valores = list(array_total_string)

            matriz = np.array(valores).reshape(rows, columns)
            # print(matriz)

            matrices_diagonales = get_diagonal_vectors_adn_mutation(matriz)
            resultado_boolean_mutacion_diagonal = detection_mutation(matrices_diagonales)
            # print(resultado_boolean_mutacion_diagonal)

            matrices_filas = get_row_vectors_adn_mutation(matriz)
            resultado_boolean_mutacion_fila = detection_mutation(matrices_filas)
            # print(resultado_boolean_mutacion_fila)

            matrices_columnas = get_col_vectors_adn_mutation(matriz)
            resultado_boolean_mutacion_columna = detection_mutation(matrices_columnas)
            # print(resultado_boolean_mutacion_columna)

            return resultado_boolean_mutacion_diagonal or resultado_boolean_mutacion_fila or resultado_boolean_mutacion_columna
        else:
            return "No se puede generar la matriz de n x n"
    else:
        return "La matriz no tiene una base de ADN correcta"

if __name__ == "__main__":    
    array_without_mutation = ["ATGCGA", "CAGTGC", "TTATTT", "AGACGG", "GCGTCA", "TCACTG"]
    array_with_mutation = ["ATGCGA", "CAGTGC", "TTATGT", "AGAAGG", "CCCCTA","TCACTG"]
    test_array_base_error =  ["ABCR", "BACR", "BCAR", "BCRA"]
    test_array_lengh_less_error =  ["ATGCAA", "ATCGA", "ATGCGA", "ATGCGA", "ATGCGA", "ATGCGA"]
    test_array_lengh_higher_error =  ["ATGCAA", "ATCGTTA", "ATGCGA", "ATGCGA", "ATGCGA", "ATGCGA"]

    print(hasMutation(array_without_mutation))
    print(hasMutation(array_with_mutation))
    print(hasMutation(test_array_base_error))
    print(hasMutation(test_array_lengh_less_error))
    print(hasMutation(test_array_lengh_higher_error))
