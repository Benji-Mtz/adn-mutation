import numpy as np

def detection_mutation(matrix):
    """
    Returns True or False depending the scanning of vectors of the matrix
    """
    mutacion = False
    slicing = []
    # print(f'matriz: {matrix}')
    # print(f'len matriz: {len(matrix)}')
    for array in matrix:
        for indice in range(len(array)):
            slicing  = array[indice: 4 + indice]
            # print(f'slice: {slicing}')
            conteo_mutation = slicing.count(array[indice])
            if conteo_mutation >= 4:
                mutacion = True
            else:
                pass
        if mutacion == True:
            return mutacion
    else:
        return False
    
def get_diagonal_vectors_adn_mutation(matrix):
    """
    Returns a array with the diagonal vectors of the matrix, with a length = ((N_row or N_cols) - 1 ) * 4 + 2
    """
    diags = [matrix[::-1,:].diagonal(i) for i in range(-matrix.shape[0]+1,matrix.shape[1])]
    diags.extend(matrix.diagonal(i) for i in range(matrix.shape[1]-1,-matrix.shape[0],-1))
    arrays_matrix_diagonal = [n.tolist() for n in diags if n.tolist() ]
    
    return arrays_matrix_diagonal

def get_row_vectors_adn_mutation(matrix):
    """
    Returns a array with the row vectors of the matrix, with a length = (N_row or N_cols)
    """
    arreglo_aux = []
    arrays_matrix_row = []

    for i in range(len(matrix)):
        arreglo_aux = []
        for j in range(len(matrix)):
            arreglo_aux.append(matrix[i][j])
        arrays_matrix_row.append(arreglo_aux)
        
    return arrays_matrix_row

def get_col_vectors_adn_mutation(matrix):
    """
    Returns a array with the column vectors of the matrix, with a length = (N_row or N_cols)
    """
    arreglo_aux = []
    arrays_matrix_col = []

    for i in range(len(matrix)):
        arreglo_aux = []
        for j in range(len(matrix)):
            arreglo_aux.append(matrix[j][i])
        arrays_matrix_col.append(arreglo_aux)
        
    return arrays_matrix_col