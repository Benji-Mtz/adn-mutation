# Correr el proyecto en local.

## Crear un entorno virtual de python 

Por ejemplo con el siguiente comando podemos crear un entorno virtual llamado env, que servira para la instalación de las dependencias del proyecto.

```
$ python -m venv env
```

## Activando el entorno virtual

Para instalar las dependencias del proyecto tendremos que activar nuestro entorno virtual así:

```
$ source env/bin/activate (linux o mac)
> ./env/Scripts/activate.bat (windows)
```

## Installation de dependencias

Ya con el entorno activado podremos ver que enfrente del simbolo de $ o el simbolo que meneje su terminal aparece el nombre del ambiente entre parentesis (env), de esa forma para instalar las dependencias bastara con ejecutar el siguiente comando que toma el archivo requirements.txt para dicha tarea:

```
(env) $ pip install -r requirements.txt
```
## Ejecución del programa base
Ejecutar el programa base servira para tener una idea de los resultados del algoritmo de las mutaciones

```
(env) $ python base.py
```
## Ejecución del programa de la API
Para ejecutar el programa de la API solamente bastara con el escribir el siguiente comando y dar enter, después podrá probar la api con postman o algún programa de su preferencia.

```
(env) $ python app.py
```