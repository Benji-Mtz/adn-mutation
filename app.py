import os
from flask import Flask, request, jsonify
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.dialects.postgresql import ARRAY
from flask_cors import CORS
from base import hasMutation

app = Flask(__name__)

# app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql+psycopg2://benji:benji@localhost/mutations'
SECRET_KEY = os.environ.get('SECRET_KEY') or 'flask-api'
app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql://hnmquwmtddyvld:9f4d39cdd20a1edbbfdeacecfe34dfe5899f6b4767bf84c666a9d4d0e3352f45@ec2-3-230-11-138.compute-1.amazonaws.com:5432/d28g8d3hlv3vhl'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

db = SQLAlchemy(app)
CORS(app)

class Mutation(db.Model):
    __tablename__ = 'mutations'
    id = db.Column(db.Integer, primary_key = True)
    dna = db.Column(ARRAY(db.String))
    mutations = db.Column(db.Integer, nullable = False)

    def __repr__(self):
        return "<Mutation %r>" % self.id
    
db.create_all()

@app.route('/')    
def index():
    return jsonify({"message":"Welcome to my site that determine if a ADN contains mutations or not"}), 200

@app.route('/mutation', methods = ['POST'])
def create_mutation():
    mutation_data = request.json
    
    dna = mutation_data['dna']    
    print(dna)

    res_mutation = hasMutation(dna)
    if res_mutation:
        mutations = 1
    else:
        mutations = 0
    mutation = Mutation(dna=dna, mutations=mutations)
    
    db.session.add(mutation)
    db.session.commit()
    
    if res_mutation:
        return jsonify({"success": res_mutation,"response":"El ADN tiene mutación"}), 200
    else:
        return jsonify({"success": res_mutation,"response":"El ADN no tiene mutación"}), 403

@app.route('/stats', methods = ['GET'])
def getstats():

    mutations = Mutation.query.all()
    sum_mutations = 0
    sum_no_mutations = 0
    
    print(mutations)
    
    if not mutations:
        return jsonify({"success": False,"response":"No hay datos"}), 403
    else:
        for mutation in mutations:    
            if mutation.mutations == 1:
                sum_mutations += 1
            elif mutation.mutations == 0:
                sum_no_mutations += 1
        
        if sum_no_mutations == 0:
            ratio = 0
        else:
            ratio = sum_mutations / sum_no_mutations

        return jsonify(
            {
                "count_mutations":sum_mutations,
                "count_no_mutation":sum_no_mutations,
                "ratio": ratio,
            }
        ), 200

if __name__ == '__main__':
  app.run(debug=True)